# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String, ForeignKey, Float

from app import db, login_manager

from app.base.util import hash_pass

class User(db.Model, UserMixin):

    __tablename__ = 'User'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(Binary)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            if property == 'password':
                value = hash_pass( value ) # we need bytes here (not plain str)
                
            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)

class Video(db.Model):

    __tablename__ = 'Videos'

    id = Column(Integer, primary_key=True)
    video_path = Column(String)
    audio_path = Column(String)
    text_path = Column(String)
    summary_path = Column(String)
    sentiment = Column(String)
    snt_score = Column(Float)
    status = Column(String)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)

class Insights(db.Model):

    __tablename__ = 'Insights'

    id = Column(Integer, primary_key=True)
    sentiment = Column(String)
    sent_score = Column(Float)
    video_id = Column(Integer, ForeignKey("Videos.id"), nullable=False)


@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()

@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None

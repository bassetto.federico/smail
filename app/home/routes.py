# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
from pathlib import Path
import threading

from app import db
from app.home import blueprint
from app.engine.text2summary import text_to_summary, classify_sentiment
from app.engine.vid2text import video_to_audio, audio_to_text
from app.base.models import Video
from flask import render_template, redirect, url_for, request, flash, current_app
from flask_login import login_required, current_user
from app import login_manager
from jinja2 import TemplateNotFound
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
from sqlalchemy.sql import select


@blueprint.route('/index')
@login_required
def index():
    recordings = db.session.query(Video).filter_by(user_id=current_user.get_id()).all()
    return render_template('summaries.html', recordings=recordings)


def video_to_summary(app, video_id, video_path, audio_path, text_path, summary_path):
    print("Started thread")
    with app.app_context() as ctx:
        video = db.session.query(Video).filter_by(id=video_id).first()
        try:
            video.status = "Converting Audio"
            db.session.merge(video)
            db.session.commit()
            print("Converting to audio")
            video_to_audio(video_path, audio_path)

            # Convert to text
            video.status = "Audio to text..."
            db.session.merge(video)
            db.session.commit()
            print("Converting to text")
            audio_to_text(audio_path, text_path, enable_punctuation=False) 
            
            # Summarize
            video.status = "Summarizing text..."
            db.session.merge(video)
            db.session.commit()
            print("Summarizing text")
            text_to_summary(text_path, summary_path)
            # classify sentiment 

            video.status = "Complete"
            try:
                snt_dict = classify_sentiment(text_path)
                video.sentiment = snt_dict[0]['label']
                video.snt_score = snt_dict[0]['score']
            except:
                pass
            db.session.merge(video)
            db.session.commit()
        except Exception as e:
            print(str(e))
            video.status = "Error"
            db.session.merge(video)
            db.session.commit()



@blueprint.route('/uploader', methods = ['GET', 'POST'])
@login_required
def upload_file():
   if request.method == 'POST':
        f = request.files['file']
        # Create video path
        video_path = Path(f"app/base/static/engine-assets/videos/{secure_filename(f.filename)}")
        # Create audio path
        audio_path = Path(f"app/base/static/engine-assets/audio/{video_path.stem}.wav")
        # Create text path
        text_path = Path(f"app/base/static/engine-assets/text/{video_path.stem}.txt")
        # summary and sentiment
        summary_path = Path(f"app/base/static/engine-assets/text/{video_path.stem}_summary.txt")
        # snt_path = Path(f"app/base/static/engine-assets/sentiment/{video_path.stem}_snt.json")

        # Store new entry
        db_record_video = {
            'video_path':  str(video_path),
            'audio_path':  str(audio_path),
            'text_path': str(text_path),
            'summary_path': str(summary_path),
            'user_id': current_user.get_id(),
            "status": "Processing..."
        }
        video_record = Video(**db_record_video)
        db.session.add(video_record)
        db.session.commit()

        # Store file and launch processing
        f.save(video_path)
        app = current_app._get_current_object()
        x = threading.Thread(target=video_to_summary, args=(
            app,
            video_record.id,
            video_path,
            audio_path,
            text_path,
            summary_path
        ))
        x.start()

        return redirect(url_for('.index', video_id=video_record.id))


@blueprint.route('/visualize-results/<int:video_id>', methods = ['GET', 'POST'])
@login_required
def visualize_result(video_id):
    video = db.session.query(Video).filter_by(id=video_id).first()
    text_file_path = video.text_path
    summary_file_path = video.summary_path
    video_path=video.video_path.replace('app/base', '')
    with open(text_file_path, 'r') as fp:
        text = fp.read()

    with open(summary_file_path, 'r') as fp:
        summary = fp.read()

    return render_template('results.html', text=text, summary=summary, video_path=video_path)


@blueprint.route('/<template>')
@login_required
def route_template(template):

    try:

        if not template.endswith( '.html' ):
            template += '.html'

        # Detect the current page
        segment = get_segment( request )

        # Serve the file (if exists) from app/templates/FILE.html
        return render_template( template, segment=segment )

    except TemplateNotFound:
        return render_template('page-404.html'), 404
    
    except:
        return render_template('page-500.html'), 500

# Helper - Extract current page name from request 
def get_segment( request ): 

    try:

        segment = request.path.split('/')[-1]

        if segment == '':
            segment = 'index'

        return segment    

    except:
        return None  

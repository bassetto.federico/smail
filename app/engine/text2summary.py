import json
from pathlib import Path

from transformers import (AutoModelForCausalLM, AutoModelForSeq2SeqLM,
                          AutoModelWithLMHead, AutoTokenizer, pipeline)

ROOT_PATH = Path(__file__).parents[2]

## Public methods 
def text_to_summary(input_path: Path, output_path: Path):
    summary = _text_to_summary_2(input_path)
    with open(output_path, "w") as f:
        f.write(summary)

def classify_sentiment(input_path: Path, output_path: Path=None):
    f = open(input_path, "r", encoding="utf8")
    to_tokenize = f.read()
    # trim end of file
    to_tokenize = to_tokenize[:2048]

    # Initialize the HuggingFace summarization pipeline
    cl = pipeline("sentiment-analysis")
    sentiment_dict = cl(to_tokenize)
    return sentiment_dict

### private methods
def _text_to_summary_1(input_path: Path):
    model = AutoModelForSeq2SeqLM.from_pretrained("facebook/bart-base")
    tokenizer = AutoTokenizer.from_pretrained("facebook/bart-base")

    f = open(input_path, "r", encoding="utf8")
    ARTICLE = f.read()
    # T5 uses a max_length of 512 so we cut the article to 512 tokens.
    inputs = tokenizer.encode("summarize: " + ARTICLE, return_tensors="pt", max_length=512, truncation=True)
    max_length = int(len(inputs[0]) * 0.7) # half of inputs length? 
    outputs = model.generate(inputs, max_length=max_length, min_length=40, length_penalty=1.0, num_beams=4, early_stopping=True)
    summary = tokenizer.decode(outputs[0])
    return summary


def _text_to_summary_2(input_path: Path):
    # Open and read the article
    f = open(input_path, "r", encoding="utf8")
    to_tokenize = f.read()

    # Initialize the HuggingFace summarization pipeline
    summarizer = pipeline("summarization")
    summarized = summarizer(to_tokenize, min_length=75, max_length=512)
    return summarized[0]['summary_text']


# example main
if __name__ == '__main__':
    text_file = ROOT_PATH/"app/base/static/engine-assets/text/job-1.txt"
    summarized = _text_to_summary_2(text_file)
    # Print summarized text
    print("summarizing")
    print(summarized)

    snt = classify_sentiment(text_file, 'out')
    
from pathlib import Path

import moviepy.editor as mp
import speech_recognition as sr
import io
import json
from google.cloud import speech, storage
from google.oauth2 import service_account
from flask import current_app

ROOT_PATH = Path(__file__).parents[2]


def video_to_audio(input_path: Path, output_path: Path):
    my_clip = mp.VideoFileClip(str(input_path))
    my_clip.audio.write_audiofile(str(output_path))
    return True

def transcribe_file(speech_file):
    """Transcribe the given audio file asynchronously."""
    app = current_app._get_current_object()
    with io.open(f"{app.config.root_path}/../google_credentials.json", "r") as fp:
        owner_credentials = json.load(fp)  

    credentials_speech = service_account.Credentials.from_service_account_info(owner_credentials)
    credentials_bucket = service_account.Credentials.from_service_account_info(owner_credentials)

    speech_client = speech.SpeechClient(credentials=credentials_speech)
    storage_client = storage.Client(credentials=credentials_bucket)

    bucket = storage_client.bucket("wav-files-smail")
    blob = bucket.blob(f"{speech_file.name}")
    blob.upload_from_filename(str(speech_file))

    audio = speech.RecognitionAudio(uri=f"gs://wav-files-smail/{speech_file.name}")

    config = speech.RecognitionConfig(
        language_code="en-US",
        enable_automatic_punctuation=True,
        audio_channel_count=2
    )

    operation = speech_client.long_running_recognize(
        config=config, 
        audio=audio)

    print("Waiting for speech to text operation to complete...")
    response = operation.result(timeout=90)

    all_text = []
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        print(u"Transcript: {}".format(result.alternatives[0].transcript))
        print("Confidence: {}".format(result.alternatives[0].confidence))
        all_text.append(result.alternatives[0].transcript)
    return " ".join(all_text)


def audio_to_text(input_path: Path, output_path: Path, enable_punctuation=False):
    filetype = input_path.suffix
    assert filetype == ".wav"

    all_text = transcribe_file(input_path)
    with open(output_path, "w") as text_file:
        text_file.write(all_text)
    return True


if __name__ == "__main__":
    video_file = ROOT_PATH/"app/base/static/engine-assets/videos/class.mp4"
    audio_file = ROOT_PATH/"app/base/static/engine-assets/audio/class.wav"
    text_file = ROOT_PATH/"app/base/static/engine-assets/text/class.txt"

    print("video to audio")
    result = video_to_audio(video_file, audio_file)
    print("audio to text")
    result = audio_to_text(audio_file, text_file)
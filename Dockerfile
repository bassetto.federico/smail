FROM python:3.6

ENV FLASK_APP run.py

COPY run.py gunicorn-cfg.py requirements.txt config.py .env ./
COPY app app
COPY INTERSPEECH-T-BRNN-pre.pcl INTERSPEECH-T-BRNN-pre.pcl
COPY py_lib py_lib
COPY google_credentials.json google_credentials.json

RUN pip install -r requirements.txt

EXPOSE 5005
CMD ["gunicorn", "--config", "gunicorn-cfg.py", "run:app"]
